resource "helm_release" "metrics_server" {
  name = "metrics-server"
  chart = "stable/metrics-server"
  version = "2.0.4"
  namespace = "metrics"
}


resource "helm_release" "kube_state_metrics" {
  name = "kube-state-metrics"
  chart = "stable/kube-state-metrics"
  namespace = "kube-state-metrics"
}


data "template_file" metric_values {

  template = "${file("${path.module}/metricbeat_values.yaml")}"
  vars = {
    elk_host = "${data.terraform_remote_state.eks.elk_host}"
  }
}



resource "helm_release" "metricbeat" {
  name = "metricbeat"
  chart = "stable/metricbeat"
  namespace = "kube-state-metrics"

  values = ["${data.template_file.metric_values.rendered}"]




}
