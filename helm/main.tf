provider "aws" {
  region = "${var.region}"
}

provider "helm" {
  kubernetes {
    config_path = "${var.kubeconfig_path}"
  }
  version = ">= 0.8.0"
  namespace = "tiller-helm"
  service_account = "tiller"
}


