resource "helm_release" "cluster_autoscaler" {
  name      = "cluster-autoscaler"
  chart     = "stable/cluster-autoscaler"
  namespace = "autoscaler"

  set {
    name = "autoDiscovery.clusterName"
    value = "nutellinoplayground-cross-kubernetes-cluster"
  }

  set {
    name = "sslCertPath"
    value = "/etc/kubernetes/pki/ca.crt"
  }

  set {
    name ="rbac.create"
    value = "true"
  }

  set {
    name ="awsRegion"
    value = "${var.region}"
  }

}