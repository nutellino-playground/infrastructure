resource "helm_release" "spot_handler" {
  name      = "spot-handler"
  chart     = "stable/k8s-spot-termination-handler"
  namespace = "spot"
}