terraform {
  backend "s3" {
    bucket = "terraform.nutellino-playground"
    key    = "terraform/helm/tfstate"
    region = "eu-west-1"
  }
}


data "terraform_remote_state" "eks" {
  backend = "s3"
  config {
    bucket = "terraform.nutellino-playground"
    key    = "terraform/eks/tfstate"
    region = "eu-west-1"
  }
}

