data "helm_repository" "kiwigrid" {
  name = "kiwigrid"
  url  = "https://kiwigrid.github.io"
}


resource "helm_release" "logging" {
  name      = "logging"
  chart     = "kiwigrid/fluentd-elasticsearch"
  namespace = "logging"
  set {
    name  = "elasticsearch.host"
    value = "${data.terraform_remote_state.eks.elk_host}"
  }
  set {
    name  = "elasticsearch.port"
    value = "9200"
  }
  set {
    name ="elasticsearch.logstash_prefix"
    value = "kubernetes"
  }
  set {
    name ="elasticsearch.auth.enabled"
    value = "true"
  }

  set {
    name ="image.repository"
    value = "gcr.io/google-containers/fluentd-elasticsearch"

  }
  set {
    name ="image.tag"
    value = "v2.4.0"

  }
  set {
    name ="elasticsearch.auth.user"
    value = "elastic"
  }

  set {
    name ="elasticsearch.auth.password"
    value = "changeme"
  }

  set {
    name = "elasticsearch.buffer_queue_limit"
    value = "1000"
  }
  set {
    name = "elasticsearch.buffer_chunk_limit"
    value = "10M"
  }
  set {
    name = "elasticsearch.buffer_chunk_limit"
    value = "10M"
  }

  set {
    name = "resources.limits.cpu"
    value = "100m"
  }
  set {
    name = "resources.limits.memory"
    value = "200Mi"
  }
  set {
    name = "resources.requests.cpu"
    value = "100m"
  }
  set {
    name = "resources.requests.memory"
    value = "200Mi"
  }
}