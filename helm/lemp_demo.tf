resource "helm_release" "lemp_demo" {
  name      = "lemp-demo"
  chart     = "https://nutellino-playground.gitlab.io/charts/lemp-demo-0.0.12.tgz"
  namespace = "lemp-demo"
}