# from red to blue

resource "aws_security_group_rule" "ingress_workers_dev_hot_to_cold" {
  description              = "Allow the workers to communicate with each other"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "-1"
  source_security_group_id = "${module.eks_dev_workers_red.security_group_id}"
  security_group_id        = "${module.eks_dev_workers_blue.security_group_id}"
  type                     = "ingress"
}

# from blue to red

resource "aws_security_group_rule" "ingress_workers_dev_cold_to_hot" {
  description              = "Allow the workers to communicate with each other"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "-1"
  source_security_group_id = "${module.eks_dev_workers_blue.security_group_id}"
  security_group_id        = "${module.eks_dev_workers_red.security_group_id}"
  type                     = "ingress"
}

resource "aws_security_group_rule" "allow_all_dev_red_vpc" {
  type              = "ingress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  description       = "VPC "
  cidr_blocks       = ["198.19.0.0/16"]
  security_group_id = "${module.eks_dev_workers_red.security_group_id}"
}

resource "aws_security_group_rule" "allow_all_dev_blue_vpc" {
  type              = "ingress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  description       = "VPC "
  cidr_blocks       = ["198.19.0.0/16"]
  security_group_id = "${module.eks_dev_workers_blue.security_group_id}"
}
