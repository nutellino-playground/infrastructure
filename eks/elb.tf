resource "aws_security_group" "application_alb_sg" {
  name = "nutellinoplayground-alb-sg"

  description = "nutellinoplayground-alb-sg"
  vpc_id      = "${module.vpc.vpc_id}"

  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "application_alb_allow_443" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "-1"
  description       = "All HTTPS"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.application_alb_sg.id}"
}

resource "aws_alb" "application_lb" {
  internal           = false
  name               = "nutellinoplayground-alb"
  subnets            = ["${module.subnets.public_subnet_ids}"]
  security_groups    = ["${aws_security_group.application_alb_sg.id}"]
  load_balancer_type = "application"
  tags               = "${var.tags_all}"
}

resource "aws_alb_listener" "alb_application_listener" {
  load_balancer_arn = "${aws_alb.application_lb.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${aws_acm_certificate.lempdemo_certificate.arn}"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.default_application_alb.arn}"
  }
}

data "aws_route53_zone" lempdemo_zone {
  name = "${var.lemp_demo_domain}"
}

resource "aws_acm_certificate" "lempdemo_certificate" {
  domain_name               = "${var.lemp_demo_domain}"
  validation_method         = "DNS"
  subject_alternative_names = ["*.${var.lemp_demo_domain}"]

  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_route53_record" "lempdemo_cert_validation" {
  name    = "${aws_acm_certificate.lempdemo_certificate.domain_validation_options.0.resource_record_name}"
  type    = "${aws_acm_certificate.lempdemo_certificate.domain_validation_options.0.resource_record_type}"
  zone_id = "${data.aws_route53_zone.lempdemo_zone.id}"
  records = ["${aws_acm_certificate.lempdemo_certificate.domain_validation_options.0.resource_record_value}"]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "lempdemo_cert" {
  certificate_arn = "${aws_acm_certificate.lempdemo_certificate.arn}"

  validation_record_fqdns = [
    "${aws_route53_record.lempdemo_cert_validation.fqdn}",
  ]
}

resource "aws_alb_target_group" "default_application_alb" {
  name     = "application-alb"
  port     = "8080"
  protocol = "HTTP"
  vpc_id   = "${module.vpc.vpc_id}"
}




resource "aws_route53_record" "lempdemo-A"{
  zone_id = "${data.aws_route53_zone.lempdemo_zone.id}"
  name    = "lempdemo.${var.lemp_demo_domain}"
  type    = "CNAME"
  records = ["${aws_alb.application_lb.dns_name}"]
  ttl     = "300"
}



module "eks_lempdemo" {

  source = "git::https://gitlab.com/nutellino-playground/tf-modules/http-target-group.git?ref=0.0.2"
  name = "eks-lempdemo"
  alb_listener_arn = "${aws_alb_listener.alb_application_listener.arn}"
  alb_host = "lempdemo.${var.lemp_demo_domain}"
  alb_path = "/*"
  autoscaling_group_name = "${module.eks_dev_workers_red.autoscaling_group_name}"
  protocol = "HTTP"
  port = "30000"
  vpc_id = "${module.vpc.vpc_id}"
  health_check_protocol = "HTTP"
  health_check_matcher  = "200"
  health_check_healthy_threshold = "2"
  health_check_unhealthy_threshold = "2"
  health_check_path     = "/"
  health_check_timeout  = "5"
  health_check_interval = "10"

}