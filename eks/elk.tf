module "elk-instance" {
  source                      = "git::https://gitlab.com/nutellino-playground/tf-modules/ec2.git?ref=0.0.2"
  ssh_key_pair                = "${var.key_name}"
  instance_type               = "t3.medium"
  vpc_id                      = "${module.vpc.vpc_id}"
  security_groups             = ["${aws_security_group.elk-instance.id}"]
  subnet                      = "${module.subnets.public_subnet_ids[0]}"
  name                        = "elk"
  ami                         = "ami-0c0a804957898c597"
  ami_owner                   = "801119661308"
  namespace                   = "nutellinoplayground"
  stage                       = "cross"

}


resource "null_resource" "install-prerequisites" {
  triggers {
    host = "${module.elk-instance.public_dns}"
  }
  connection {
    host = "${module.elk-instance.public_dns}"
    type = "ssh"
    user = "ec2-user"
    private_key = "${file(var.local_key_path)}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum install git -y",
      "sudo amazon-linux-extras install docker -y",
      "sudo service docker start",
      "sudo chkconfig docker on",
      "sudo usermod -a -G docker ec2-user",
      "sudo curl -L \"https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)\" -o /usr/local/bin/docker-compose",
      "sudo chmod +x /usr/local/bin/docker-compose"


    ]
  }
}


resource "null_resource" "install-elk" {
  depends_on = ["null_resource.install-prerequisites"]
  triggers {
    host = "${module.elk-instance.public_dns}"
  }
  connection {
    host = "${module.elk-instance.public_dns}"
    type = "ssh"
    user = "ec2-user"
    private_key = "${file(var.local_key_path)}"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir /home/ec2-user/elk/",
      "cd /home/ec2-user/elk/",
      "git clone https://github.com/deviantony/docker-elk.git .",
      "echo \"ELK_VERSION=6.8.0\" > .env",
      "docker-compose up -d"

    ]
  }
}

resource "aws_security_group" "elk-instance" {
  name = "elk-instance"

  description = "elk-instance"
  vpc_id = "${module.vpc.vpc_id}"


  # Allow all outbound traffic.
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


# Security group Rules

resource "aws_security_group_rule" "allow_kubernetes" {

  type            = "ingress"
  from_port       = 0
  to_port         = 0
  protocol        = "-1"
  cidr_blocks     = ["${module.vpc.vpc_cidr_block}"]
  description = "k8s VPC permit"
  security_group_id = "${aws_security_group.elk-instance.id}"
}


resource "aws_security_group_rule" "allow_managing" {

  type            = "ingress"
  from_port       = 0
  to_port         = 0
  protocol        = "-1"
  cidr_blocks     = ["${var.management_cidr}"]
  description = "Managing permit"
  security_group_id = "${aws_security_group.elk-instance.id}"
}