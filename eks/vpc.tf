locals {
  # The usage of the specific kubernetes.io/cluster/* resource tags below are required
  # for EKS and Kubernetes to discover and manage networking resources
  # https://www.terraform.io/docs/providers/aws/guides/eks-getting-started.html#base-vpc-networking
  tags = "${merge(var.tags_all, map("kubernetes.io/cluster/${var.cluster_name}", "shared"))}"
}

module "vpc" {
  source     = "git::https://gitlab.com/nutellino-playground/tf-modules/vpc.git?ref=0.0.1"
  namespace  = "nutellinoplayground"
  stage      = "cross"
  name       = "kubernetes"
  tags       = "${local.tags}"
  cidr_block = "198.19.0.0/16"
}

data "aws_availability_zones" "available" {}

module "subnets" {
  source              = "git::https://gitlab.com/nutellino-playground/tf-modules/vpc-subnet.git?ref=0.0.1"
  availability_zones  = ["${data.aws_availability_zones.available.names}"]
  namespace           = "nutellinoplayground"
  stage               = "cross"
  name                = "kubernetes"
  tags                = "${local.tags}"
  region              = "${var.region}"
  vpc_id              = "${module.vpc.vpc_id}"
  igw_id              = "${module.vpc.igw_id}"
  cidr_block          = "${module.vpc.vpc_cidr_block}"
  nat_gateway_enabled = "false"
}
