variable "tags_all" {
  type = "map"

  default = {
    Terraform = "yes"
    Cluster   = "nutellinoplayground-cross-kubernetes-cluster"
    Customer  = "nutellino"
  }
}

variable "eks_ami" {
  type    = "string"
  default = "ami-098fb7e9b507904e7"
}

data "template_file" "alb_policy" {
  template = "${file("${path.module}/policies/alb-policy.json")}"
}

data "template_file" "autoscaling_policy" {
  template = "${file("${path.module}/policies/autoscaling-policy.json")}"
}
