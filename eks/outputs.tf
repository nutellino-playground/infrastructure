output "elk_host" {
  value = "${module.elk-instance.private_dns}"
}

output "kubeconfig" {
  sensitive = true
  value = "${module.eks_cluster.kubeconfig}"
}