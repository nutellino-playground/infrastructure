
variable "namespace_kube_system" {
  type = "string"
  default = "kube-system"
}


data "template_file" "configmap_workers" {
  template = "${file("./configmap_workers_tpl.yaml")}"
  vars {
    aws_account_id = "${var.aws_account_id}"
    aws_account_username = "${var.aws_account_username}"
  }
}



resource "null_resource" "setup_configmap" {
  provisioner "local-exec" {
    command = "cat > configmap_workers.yaml <<EOL\n${data.template_file.configmap_workers.rendered}\nEOL"
  }
}


resource "null_resource" "setup_k8s" {
  depends_on = ["null_resource.setup_configmap"]
  triggers {
    kubeconfig = "${module.eks_cluster.kubeconfig}"
  }

  provisioner "local-exec" {
    command = "mkdir -p ~/.kube/ && echo \"${module.eks_cluster.kubeconfig}\" > ${var.kubeconfig_path}"
  }

  provisioner "local-exec" {
    command = "KUBECONFIG=${var.kubeconfig_path} kubectl -n ${var.namespace_kube_system} apply -f ${path.module}/configmap_workers.yaml"
  }

  provisioner "local-exec" {
    command = "KUBECONFIG=${var.kubeconfig_path} kubectl apply -f ${path.module}/system_deployments/helm/"
  }

  provisioner "local-exec" {
    command = "KUBECONFIG=${var.kubeconfig_path} kubectl delete storageclasses gp2"
  }

  provisioner "local-exec" {
    command = "KUBECONFIG=${var.kubeconfig_path} kubectl apply -f ${path.module}/system_deployments/storageclasses/"
  }

}