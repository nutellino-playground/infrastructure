terraform {
  backend "s3" {
    bucket = "terraform.nutellino-playground"
    key    = "terraform/eks/tfstate"
    region = "eu-west-1"
  }
}