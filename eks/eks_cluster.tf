variable "cluster_name" {
  type    = "string"
  default = "nutellinoplayground-cross-kubernetes-cluster"
}

module "eks_cluster" {
  source             = "git::https://gitlab.com/nutellino-playground/tf-modules/eks-cluster.git?ref=0.0.2"
  namespace          = "nutellinoplayground"
  stage              = "cross"
  name               = "kubernetes"
  kubernetes_version = "1.12"
  tags               = "${var.tags_all}"
  vpc_id             = "${module.vpc.vpc_id}"
  subnet_ids         = ["${module.subnets.public_subnet_ids}"]
}
