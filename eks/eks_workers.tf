#_______   ___________    ____
#|       \ |   ____\   \  /   /
#|  .--.  ||  |__   \   \/   /
#|  |  |  ||   __|   \      /
#|  '--'  ||  |____   \    /
#|_______/ |_______|   \__/

variable "tags_dev_red" {
  type = "map"

  default = {
    Terraform = "yes"
    Cluster   = "nutellinoplayground-cross-kubernetes-cluster"
    Customer  = "nutellino"
    Group     = "red"
  }
}

resource "aws_security_group_rule" "ingress_workers_dev_red" {
  description              = "Allow the cluster to receive communication from the worker nodes"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "-1"
  source_security_group_id = "${module.eks_dev_workers_red.security_group_id}"
  security_group_id        = "${module.eks_cluster.security_group_id}"
  type                     = "ingress"
}

module "eks_dev_workers_red" {
  source                                  = "git::https://gitlab.com/nutellino-playground/tf-modules/eks-workers.git?ref=0.0.3"
  namespace                               = "nutellinoplayground"
  stage                                   = "dev"
  name                                    = "kubernetes-red"
  tags                                    = "${merge(var.tags_dev_red, map("k8s.io/cluster-autoscaler/enabled", "managed"))}"
  #image_id                                = "${var.eks_ami}"
  #use_custom_image_id                     = "true"
  instance_type                           = "t3.medium"
  instance_type_secondary                 = "t2.medium"
  ondemand_instances                      = "0"
  ondemand_percentage_above_base_capacity = "0"
  vpc_id                                  = "${module.vpc.vpc_id}"
  subnet_ids                              = ["${module.subnets.public_subnet_ids}"]
  min_size                                = "3"
  max_size                                = "5"
  associate_public_ip_address             = "true"
  cluster_name                            = "${var.cluster_name}"
  cluster_endpoint                        = "${module.eks_cluster.eks_cluster_endpoint}"
  cluster_certificate_authority_data      = "${module.eks_cluster.eks_cluster_certificate_authority_data}"
  cluster_security_group_id               = "${module.eks_cluster.security_group_id}"
  key_name                                = "${var.key_name}"

  autoscaling_policies_enabled = "false"
  bootstrap_extra_args         = "--kubelet-extra-args '--node-labels=cluster=dev,group=red,node-role.kubernetes.io/red-dev,node-role.kubernetes.io/spot-worker=true'"
}

resource "aws_iam_role_policy" "attach_alb_dev_red" {
  name   = "${module.eks_dev_workers_red.autoscaling_group_name}_iam_role_policy"
  role   = "${module.eks_dev_workers_red.workers_role_id}"
  policy = "${data.template_file.alb_policy.rendered}"
}

resource "aws_iam_role_policy" "attach_autoscaling_dev_red" {
  name   = "${module.eks_dev_workers_red.autoscaling_group_name}_iam_role_policy_autoscaling"
  role   = "${module.eks_dev_workers_red.workers_role_id}"
  policy = "${data.template_file.autoscaling_policy.rendered}"
}

variable "tags_dev_blue" {
  type = "map"

  default = {
    Terraform = "yes"
    Cluster   = "nutellinoplayground-cross-kubernetes-cluster"
    Customer  = "nutellino"
    Group     = "blue"
  }
}

resource "aws_security_group_rule" "ingress_workers_dev_blue" {
  description              = "Allow the cluster to receive communication from the worker nodes"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "-1"
  source_security_group_id = "${module.eks_dev_workers_blue.security_group_id}"
  security_group_id        = "${module.eks_cluster.security_group_id}"
  type                     = "ingress"
}

module "eks_dev_workers_blue" {
  source                                  = "git::https://gitlab.com/nutellino-playground/tf-modules/eks-workers.git?ref=0.0.3"
  namespace                               = "nutellinoplayground"
  stage                                   = "dev"
  name                                    = "kubernetes-blue"
  tags                                    = "${merge(var.tags_dev_blue, map("k8s.io/cluster-autoscaler/enabled", "managed"))}"
  #image_id                                = "${var.eks_ami}"
  #use_custom_image_id                     = "true"
  instance_type                           = "t3.medium"
  instance_type_secondary                 = "t2.medium"
  ondemand_instances                      = "0"
  ondemand_percentage_above_base_capacity = "100"
  vpc_id                                  = "${module.vpc.vpc_id}"
  subnet_ids                              = ["${module.subnets.public_subnet_ids}"]
  min_size                                = "1"
  max_size                                = "5"
  associate_public_ip_address             = "true"
  cluster_name                            = "${var.cluster_name}"
  cluster_endpoint                        = "${module.eks_cluster.eks_cluster_endpoint}"
  cluster_certificate_authority_data      = "${module.eks_cluster.eks_cluster_certificate_authority_data}"
  cluster_security_group_id               = "${module.eks_cluster.security_group_id}"
  key_name                                = "${var.key_name}"

  autoscaling_policies_enabled = "false"
  bootstrap_extra_args         = "--kubelet-extra-args '--node-labels=cluster=dev,group=blue,node-role.kubernetes.io/blue-dev'"
}

resource "aws_iam_role_policy" "attach_alb_dev_blue" {
  name   = "${module.eks_dev_workers_blue.autoscaling_group_name}_iam_role_policy"
  role   = "${module.eks_dev_workers_blue.workers_role_id}"
  policy = "${data.template_file.alb_policy.rendered}"
}

resource "aws_iam_role_policy" "attach_autoscaling_dev_blue" {
  name   = "${module.eks_dev_workers_blue.autoscaling_group_name}_iam_role_policy_autoscaling"
  role   = "${module.eks_dev_workers_blue.workers_role_id}"
  policy = "${data.template_file.autoscaling_policy.rendered}"
}
