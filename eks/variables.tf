variable "region" {}
variable "aws_account_id"{}
variable "aws_account_username"{}
variable "lemp_demo_domain"{}
variable "kubeconfig_path" {}
variable "management_cidr" {}
variable "key_name" {}
variable "local_key_path" {}