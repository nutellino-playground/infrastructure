# Example Infrastructure reference

AWS Example to deploy EKS Cluster.

## EKS project

Terraform project to manage EKS with autoscaling group

## HELM project

Helm project to deploy charts on EKS cluster